package fourth.iface;
public interface Function {
    double calculation(double x);
    double getLowerLimit();
    double getUpperLimit();
}