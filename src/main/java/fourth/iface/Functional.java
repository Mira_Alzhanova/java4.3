package fourth.iface;

public interface Functional <T extends Function> {
    public double calculate(T f);
}