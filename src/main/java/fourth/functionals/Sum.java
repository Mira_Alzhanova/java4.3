package fourth.functionals;

import fourth.iface.Function;
import fourth.iface.Functional;

public class Sum<T extends Function> implements Functional <T>{

    @Override
    public double calculate(Function f) {
        return f.calculation(f.getLowerLimit()) +
                f.calculation(f.getUpperLimit()) +
                f.calculation((f.getUpperLimit() + f.getLowerLimit()) / 2);
    }
}


