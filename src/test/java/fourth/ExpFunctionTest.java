package fourth;

import fourth.functions.ExpFunction;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExpFunctionTest {

    private static final double DELTA = 1e-9;

    @Test
    public void calculation() {
        ExpFunction f = new ExpFunction();
        f.setLimits(0,5);
        f.setCoefficients(1,0);
        double x = 0;
        double y = 1;
        assertEquals(y, f.calculation(x), DELTA);
    }

    @Test
    public void calculation1() {
        ExpFunction f = new ExpFunction();
        f.setLimits(0,5);
        f.setCoefficients(1,0);
        double x = 1;
        double y = 2.718281828;
        assertEquals(y, f.calculation(x), DELTA);
    }

    @Test
    public void calculation2() {
        ExpFunction f = new ExpFunction();
        f.setLimits(0,5);
        f.setCoefficients(1,2);
        double x = 0.2;
        double y = 3.2214027582;
        assertEquals(y, f.calculation(x), DELTA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void falseLimits() {
        ExpFunction f = new ExpFunction();
        f.setLimits(3,2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void falseCoefficients() {
        ExpFunction f = new ExpFunction();
        f.setCoefficients(0, 7.5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void xDoesNotBelongToTheSegment() {
        ExpFunction f = new ExpFunction();
        f.setCoefficients(1, 7.5);
        f.setLimits(1.1,1.2);
        double x = 5;
        f.calculation(x);
    }
}