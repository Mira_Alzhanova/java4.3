package fourth;

import fourth.functions.FractionalLinearFunction;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FractionalLinearFunctionTest {

    @Test
    public void calculation1() {
        FractionalLinearFunction f = new FractionalLinearFunction();
        f.setLimits(0,5);
        f.setCoefficients(2,3, 1, 6);
        double x = 3;
        double y = 1;
        assertEquals(y, f.calculation(x), 0);
    }

    @Test
    public void calculation2() {
        FractionalLinearFunction f = new FractionalLinearFunction();
        f.setLimits(0,5);
        f.setCoefficients(0,4, 2, 0);
        double x = 4;
        double y = 0.5;
        assertEquals(y, f.calculation(x), 0);
    }


    @Test(expected = IllegalArgumentException.class)
    public void falseLimits() {
        FractionalLinearFunction f = new FractionalLinearFunction();
        f.setLimits(9,0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void falseCoefficients() {
        FractionalLinearFunction f = new FractionalLinearFunction();
        f.setCoefficients(1,2,0, 4);
    }

    @Test(expected = ArithmeticException.class)
    public void zeroDenominator() {
        FractionalLinearFunction f = new FractionalLinearFunction();
        f.setLimits(0,10);
        f.setCoefficients(1,2,1, -4);
        double x = 4;
        f.calculation(x);
    }

    @Test(expected = IllegalArgumentException.class)
    public void xDoesNotBelongToTheSegment() {
        FractionalLinearFunction f = new FractionalLinearFunction();
        f.setLimits(0,1);
        f.setCoefficients(2.5,3, 2, 1.1);
        double x = 5;
        f.calculation(x);
    }
}
