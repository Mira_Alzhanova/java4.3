package fourth;

import fourth.functions.LinearFunction;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LinearFunctionTest {

    @Test
    public void calculation1() {
        LinearFunction f = new LinearFunction();
        f.setLimits(1, 4.5);
        f.setCoefficients(2,4);
        double x = 2.5;
        double y = 9;
        assertEquals(y, f.calculation(x), 0);
    }

    @Test
    public void calculation2() {
        LinearFunction f = new LinearFunction();
        f.setLimits(-10, -3);
        f.setCoefficients(0,7);
        double x = -3.5;
        double y = 7;
        assertEquals(y, f.calculation(x), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void falseLimits() {
        LinearFunction f = new LinearFunction();
        f.setLimits(2.5,0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void xDoesNotBelongToTheSegment() {
        LinearFunction f = new LinearFunction();
        f.setLimits(0,1);
        f.setCoefficients(2.5,3);
        double x = 5;
        f.calculation(x);
    }
}
